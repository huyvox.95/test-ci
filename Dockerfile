FROM node:16.9.0-alpine3.14
COPY ./ ./src
WORKDIR /src
RUN npm install
RUN cat app/views/_header.html
EXPOSE 3000
CMD ["npm", "start"]
